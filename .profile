export EDITOR=/usr/bin/emacs
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
PATH="$HOME/tj/scripts:$PATH"
PATH=$PATH:$HOME/.emacs.d/bin 
xmodmap ~/.Xmodmap
