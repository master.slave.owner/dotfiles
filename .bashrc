#
# ~/.bashrc
#

# Prompt decoration, confuguration

#################################################
#pTIME="\[\e[1;32m\]\A" # Printing the time 
#pUSER="\[\e[0;01m\] \u" # User tj in this case
#pROOT="\[\e[1;31m\]@\[\e[0;01m\]\h " # First \[\] to make the text RED, only for @, next is to make it white for root text(tj-tonk). \h indictes hostname(computer name)
#pPWD="\[\e[1;31m\]\W\[\e[1;36\] $\[\e[0;00m\] "
##################################################
#PS1="${pTIME}${pUSER}${pROOT}${pPWD}"
##################################################

#PS1="\[\e[32m\]\u\[\e[1;37m@\e[m\e[34m\]\h\[\e[1;33m\]\w\e[1;37m\]$ "
#PS1="\[\e[32m\]\u\[\e[1;37m@\e[m\e[34m\]\h\[\e[1;33m\]\w\e[1;37m\] $\[\e[0;00m\] "
#PS1="\[\e[1;32m\]\A \[\e[0;01m\] \u\[\e[1;31m\]@\[\e[0;01m\]\h \[\e[1;31m\]\W\[\e[1;36m\] $\[\e[0;00m\] "
#PS1="\[\033[38;5;208m\]\u\[$(tput sgr0)\]\[\033[38;5;8m\]@\[$(tput sgr0)\]\[$(tput bold)\]\w\[$(tput sgr0)\]\[\033[38;5;196m\]:\[$(tput sgr0)\] -\[$(tput sgr0)\]\[\033[38;5;64m\]>\[$(tput sgr0)\] "
#PS1="\[$(tput bold)\]\[\033[38;5;102m\]\w\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;208m\]>\[$(tput sgr0)\] " # grey directory, orange >
PS1="\[$(tput bold)\]\[\033[38;5;11m\]\w\[$(tput sgr0)\] >\[$(tput sgr0)\] " # yellow directory, white >
#PS1="\[$(tput bold)\]\[\033[38;5;11m\]\w\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;0m\]>\[$(tput sgr0)\] " # yellow directory, black >
# [[ $- != *i* ]] && return

#colors() {
#	local fgc bgc vals seq0
#
#	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
#	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
#	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
#	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"
#
#	# foreground colors
#	for fgc in {30..37}; do
#		# background colors
#		for bgc in {40..47}; do
#			fgc=${fgc#37} # white
#			bgc=${bgc#40} # black
#
#			vals="${fgc:+$fgc;}${bgc}"
#			vals=${vals%%;}
#
#			seq0="${vals:+\e[${vals}m}"
#			printf "  %-9s" "${seq0:-(default)}"
#			printf " ${seq0}TEXT\e[m"
#			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
#		done
#		echo; echo
#	done
#}

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# Change the window title of X terminals
#case ${TERM} in
#	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
#		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
#		;;
#	screen*)
#		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
#		;;
#esac

use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
#safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
#match_lhs=""
#[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
#[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
#[[ -z ${match_lhs}    ]] \
#	&& type -P dircolors >/dev/null \
#	&& match_lhs=$(dircolors --print-database)
#[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true
#
#if ${use_color} ; then
#	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
#	if type -P dircolors >/dev/null ; then
#		if [[ -f ~/.dir_colors ]] ; then
#			eval $(dircolors -b ~/.dir_colors)
#		elif [[ -f /etc/DIR_COLORS ]] ; then
#			eval $(dircolors -b /etc/DIR_COLORS)
#		fi
#	fi
#
#	if [[ ${EUID} == 0 ]] ; then
#		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
#	else
#		PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
#	fi
#
#	#alias ls='ls --color=auto'
#	alias grep='grep --colour=auto'
#	alias egrep='egrep --colour=auto'
#	alias fgrep='fgrep --colour=auto'
#else
#	if [[ ${EUID} == 0 ]] ; then
#		# show root@ when we don't have colors
#		PS1='\u@\h \W \$ '
#	else
#		PS1='\u@\h \w \$ '
#	fi
#fi

unset use_color safe_term match_lhs sh

alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano -w PKGBUILD'
alias nvlc='nvlc --no-color'
alias more=less
alias ls=lsd
alias gh='history|grep'                   # can search for words in history using grep and a variable.
alias ..="cd .."
alias ..2="cd ../.."
alias ..3="cd ../../.."
#alias ..="cd ../../../"
#alias ..="cd ../../../.."
#alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias config='/usr/bin/git --git-dir=/home/tj/dotfiles/ --work-tree=/home/tj/'
                                          # py-help (keyword) gives you result of that keyword
py-help38() { python3.8 -c "help('$1')" ;}
calc() { python -ic "from __future__ import division; \
                     from math import *; \
                     from random import *" ;}



# xhost +local:root > /dev/null 2>&1 ((( Check this one out )))

# complete -cf sudo

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases

# export QT_SELECT=4

# Enable history appending instead of overwriting.  #139609
shopt -s histappend

#
# # ex - archive extractor
# # usage: ex <file>
#ex ()
#{
#  if [ -f $1 ] ; then
#    case $1 in
#      *.tar.bz2)   tar xjf $1   ;;
#      *.tar.gz)    tar xzf $1   ;;
#      *.bz2)       bunzip2 $1   ;;
#      *.rar)       unrar x $1     ;;
#      *.gz)        gunzip $1    ;;
#      *.tar)       tar xf $1    ;;
#      *.tbz2)      tar xjf $1   ;;
#      *.tgz)       tar xzf $1   ;;
#      *.zip)       unzip $1     ;;
#      *.Z)         uncompress $1;;
#      *.7z)        7z x $1      ;;
#      *)           echo "'$1' cannot be extracted via ex()" ;;
#    esac
#  else
#    echo "'$1' is not a valid file"
#  fi
#}


#man() {
#    LESS_TERMCAP_md=$'\e[01;31m' \
#    LESS_TERMCAP_me=$'\e[0m' \
#    LESS_TERMCAP_se=$'\e[0m' \
#    LESS_TERMCAP_so=$'\e[01;44;33m' \
#    LESS_TERMCAP_ue=$'\e[0m' \
#    LESS_TERMCAP_us=$'\e[01;32m' \
#    command man "$@"
#}

set +o vi

#PATH="/home/tj/perl5/bin${PATH:+:${PATH}}"; export PATH;
#PERL5LIB="/home/tj/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
#PERL_LOCAL_LIB_ROOT="/home/tj/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
#PERL_MB_OPT="--install_base \"/home/tj/perl5\""; export PERL_MB_OPT;
#PERL_MM_OPT="INSTALL_BASE=/home/tj/perl5"; export PERL_MM_OPT;
#PATH="$HOME/bin:$PATH"

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi

