#!/bin/sh

ANS="$(printf "Lock\nShutdown\nReboot\nLogout" | dmenu -i -p 'System')"
case "$ANS" in
  Lock) slock;;
  Shutdown) systemctl -i poweroff;;
  Reboot) systemctl - reboot;;
  Logout) logout;;
esac
